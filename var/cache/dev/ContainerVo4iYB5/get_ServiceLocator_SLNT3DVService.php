<?php

namespace ContainerVo4iYB5;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_SLNT3DVService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.sLNT3DV' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.sLNT3DV'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'vote' => ['privates', '.errored..service_locator.sLNT3DV.App\\Entity\\Vote', NULL, 'Cannot autowire service ".service_locator.sLNT3DV": it references class "App\\Entity\\Vote" but no such service exists.'],
        ], [
            'vote' => 'App\\Entity\\Vote',
        ]);
    }
}
