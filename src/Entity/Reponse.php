<?php

namespace App\Entity;

use App\Repository\ReponseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReponseRepository::class)
 */
class Reponse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Vote::class, mappedBy="reponses")
     */
    private $vots;

    public function __construct()
    {
        $this->vots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Vote[]
     */
    public function getVots(): Collection
    {
        return $this->vots;
    }

    public function addVot(Vote $vot): self
    {
        if (!$this->vots->contains($vot)) {
            $this->vots[] = $vot;
            $vot->addReponse($this);
        }

        return $this;
    }

    public function removeVot(Vote $vot): self
    {
        if ($this->vots->contains($vot)) {
            $this->vots->removeElement($vot);
            $vot->removeReponse($this);
        }

        return $this;
    }
}
